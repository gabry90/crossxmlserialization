﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

using System;
using System.Linq;
using System.Reflection;
using System.Collections;

namespace CrossXmlSerialization.Extensions
{
    /// <summary>
    /// Operation extensions class
    /// </summary>
    internal static class OperationExtension
    {
        /// <summary>
        /// Get attributes info
        /// </summary>
        /// <typeparam name="T">Type of attribute</typeparam>
        /// <param name="type">name of attribute</param>
        /// <returns>Attribute of T</returns>
        public static T GetAttributeInfo<T>(this Type type) where T : Attribute
        {
            return (T)Attribute.GetCustomAttribute(type, typeof(T), true);
        }

        /// <summary>
        /// Call method with reflection
        /// </summary>
        /// <typeparam name="T">Type of object that contains method to call</typeparam>
        /// <param name="self">object that contains method to call</param>
        /// <param name="methodName">Method name</param>
        /// <param name="parameters">Method parameters</param>
        /// <returns>method return type and value</returns>
        public static T CallMethod<T>(this T self, string methodName, params object[] parameters)
        {
            return (T)self.GetType().GetMethod(methodName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy).Invoke(self, parameters);
        }

        /// <summary>
        /// Extended cloning object
        /// </summary>
        /// <typeparam name="T">Type of object to clone</typeparam>
        /// <param name="self">Object to clone</param>
        /// <returns>Object cloned</returns>
        public static T CloneEx<T>(this T self)
        {
            return self is ICloneable ? (T)((ICloneable)self).Clone() : (T)self.GetType().GetMethod("MemberwiseClone", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(self, null);
        }

        /// <summary>
        /// Extended disposing object
        /// </summary>
        /// <typeparam name="T">Type of object to dispose</typeparam>
        /// <param name="self">Object to dispose</param>
        public static void DisposeEx<T>(this T self)
        {
            //if is IDisposable call standard interface member
            if (self is IDisposable)
            {
                ((IDisposable)self).Dispose();
                return;
            }

            //try to dispose manually

            Type type = self.GetType();

            if (type.IsValueType())
            {
                self.SetDefaultValue();
                return;
            }

            if (type.GetInterfaces().Contains(typeof(ICollection)))
            {
                foreach (var item in (ICollection)self)
                    if (item != null)
                        item.DisposeEx();

                self.DisposeEvents();
                self.SetDefaultValue();

                return;
            }

            self.DisposeEvents();
            self.DisposeProperties();
            self.DisposeFields();
        }

        /// <summary>
        /// Disposing events
        /// </summary>
        /// <typeparam name="T">type target</typeparam>
        /// <param name="self">object target</param>
        public static void DisposeEvents<T>(this T self)
        {
            Type type = self.GetType();
            BindingFlags allFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy;

            type.GetEvents(allFlags).ToList().ForEach(x =>
            {
                FieldInfo fi = type.GetField(x.Name, allFlags);
                if (fi == null)
                    return;

                Delegate del = (Delegate)fi.GetValue(self);
                if (del != null)
                    del.GetInvocationList().ToList().ForEach(y => x.RemoveEventHandler(self, y));
            });
        }

        /// <summary>
        /// Disposing properties
        /// </summary>
        /// <typeparam name="T">type target</typeparam>
        /// <param name="self">object target</param>
        public static void DisposeProperties<T>(this T self)
        {
            Type type = self.GetType();
            BindingFlags allFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy;

            type.GetProperties(allFlags).ToList().ForEach(x =>
            {
                if (x is IDisposable)
                {
                    ((IDisposable)x).Dispose();
                    return;
                }

                //call recursively
                if (x.PropertyType.IsClass && x.PropertyType != typeof(string))
                {
                    object val = x.GetValue(self, null);
                    if (val != x.PropertyType.GetDefaultValue())
                        val.DisposeEx();
                }

                try { x.SetValue(self, x.PropertyType.GetDefaultValue(), null); }
                catch { }
            });
        }

        /// <summary>
        /// Dispose fields
        /// </summary>
        /// <typeparam name="T">Type target</typeparam>
        /// <param name="self">object target</param>
        public static void DisposeFields<T>(this T self)
        {
            Type type = self.GetType();
            BindingFlags allFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy;

            type.GetFields(allFlags).ToList().ForEach(x =>
            {
                if (x is IDisposable)
                {
                    ((IDisposable)x).Dispose();
                    return;
                }

                //call recursively
                if (x.FieldType.IsClass && x.FieldType != typeof(string))
                {
                    object val = x.GetValue(self);
                    if (val != x.FieldType.GetDefaultValue())
                        val.DisposeEx();
                }

                try { x.SetValue(self, x.FieldType.GetDefaultValue()); }
                catch { }
            });
        }
    }
}