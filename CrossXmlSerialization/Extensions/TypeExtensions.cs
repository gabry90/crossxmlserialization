﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

using System;
using System.Linq;

namespace CrossXmlSerialization.Extensions
{
    /// <summary>
    /// Type extensions class
    /// </summary>
    internal static class TypeExtensions
    {
        /// <summary>
        /// Get default value based on type
        /// </summary>
        /// <param name="type">Type target</param>
        /// <returns>Default value</returns>
        public static object GetDefaultValue(this Type type)
        {
            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        /// <summary>
        /// Set the default value
        /// </summary>
        /// <typeparam name="T">type target</typeparam>
        /// <param name="self">object target</param>
        public static void SetDefaultValue<T>(this T self)
        {
            try { self = (T)self.GetType().GetDefaultValue(); }
            catch { }
        }

        /// <summary>
        /// Check if class is same or sub class
        /// </summary>
        /// <param name="self">Type target</param>
        /// <param name="baseType">Type of base class</param>
        /// <returns>True if is same or sub class, False else</returns>
        public static bool IsSameOrSubClass(this Type self, Type baseType)
        {
            return self.IsSubclassOf(baseType) || self == baseType;
        }

        /// <summary>
        /// Check if type is value type
        /// </summary>
        /// <param name="self">Type target</param>
        /// <returns>True if is value type, False else</returns>
        public static bool IsValueType(this Type self)
        {
            return self.IsValueType || self == typeof(string) || self.IsEnum;
        }

        /// <summary>
        /// Check if type is nullable
        /// </summary>
        /// <param name="self">Type target</param>
        /// <returns>True if is nullable, False else</returns>
        public static bool IsNullable(this Type self)
        {
            if (!self.IsValueType)
                return true;

            return !self.IsValueType
                    || (self.IsGenericType
                        && self.GetGenericTypeDefinition() == typeof(Nullable<>));
        }

        /// <summary>
        /// Get base type
        /// </summary>
        /// <param name="self">type target</param>
        /// <returns>base type of type</returns>
        public static Type GetBaseType(this Type self)
        {
            return self.IsGenericType? self.GetGenericArguments().FirstOrDefault() : self.BaseType.GetGenericArguments().FirstOrDefault();
        }

        /// <summary>
        /// Get nullable type
        /// </summary>
        /// <param name="self">Type target</param>
        /// <returns>Nullable type</returns>
        public static Type GetNullableType(this Type self)
        {
            if (self == typeof(void))
                return null;

            return self.IsNullable() ? self : typeof(Nullable<>).MakeGenericType(self);
        }
    }
}