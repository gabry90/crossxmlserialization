﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

namespace CrossXmlSerialization.Extensions
{
    /// <summary>
    /// Custom property info class
    /// </summary>
    internal class CustomPropertyInfo
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public Type Type { get; set; }
        public bool IsList { get; set; }
        public bool IsValueType { get; set; }
        public List<Attribute> Attributes { get; set; }
    }

    /// <summary>
    /// Property extensions class
    /// </summary>
    internal static class PropertyExtensions
    {
        #region get methods

        /// <summary>
        /// Get properties with specified attributes
        /// </summary>
        /// <typeparam name="T">Type of class to get properties</typeparam>
        /// <param name="self">Class to get properties</param>
        /// <param name="environment">Type of environment</param>
        /// <param name="attributes">Attributes filter</param>
        /// <returns>List of custom property info with specified attributes</returns>
        public static List<CustomPropertyInfo> GetPropertiesWithAttributes<T>(this T self, SerializationEnvironment environment, params Type[] attributes) where T : class
        {
            Type type = self.GetType();

            List<CustomPropertyInfo> properties = new List<CustomPropertyInfo>();
            foreach (var property in type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy))
            {
                if (!property.CanRead)
                    continue;   //the property cannot readable

                if (attributes.Length > 0 && attributes.Count(x => Attribute.IsDefined(property, x)) != attributes.Length)
                    continue;   //no attributes match

                //get property value
                object value = environment == SerializationEnvironment.iOS
                    ? property.GetGetMethod().Invoke(self, null)
                    : property.GetValue(self, null);

                //generate custom property info class and add it to collection
                properties.Add(new CustomPropertyInfo 
                { 
                    Name = property.Name, 
                    Value = value, 
                    Type = property.PropertyType, 
                    IsList = typeof(IList).IsAssignableFrom(property.PropertyType),
                    IsValueType = property.PropertyType.IsValueType(),
                    Attributes = property.GetCustomAttributes(true).Cast<Attribute>().ToList() 
                });
            }

            return properties;
        }

        /// <summary>
        /// Get properties without specified attributes
        /// </summary>
        /// <typeparam name="T">Type of class to get properties</typeparam>
        /// <param name="self">Class to get properties</param>
        /// <param name="environment">Type of environment</param>
        /// <param name="attributes">Attributes filter</param>
        /// <returns>List of custom property info without specified attributes</returns>
        public static List<CustomPropertyInfo> GetPropertiesExcludingAttributes<T>(this T self, SerializationEnvironment environment, params Type[] attributes) where T : class
        {
            Type type = self.GetType();

            List<CustomPropertyInfo> properties = new List<CustomPropertyInfo>();
            foreach (var property in type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy))
            {
                if (!property.CanRead)
                    continue;   //the property cannot readable

                if (attributes.Length > 0 && attributes.ToList().Exists(x => Attribute.IsDefined(property, x)))
                    continue;   //attribute match

                //get property value
                object value = environment == SerializationEnvironment.iOS
                    ? property.GetGetMethod().Invoke(self, null)
                    : property.GetValue(self, null);

                //generate custom property info class and add it to collection
                properties.Add(new CustomPropertyInfo
                {
                    Name = property.Name,
                    Value = value,
                    Type = property.PropertyType,
                    IsList = typeof(IList).IsAssignableFrom(property.PropertyType),
                    IsValueType = property.PropertyType.IsValueType(),
                    Attributes = property.GetCustomAttributes(true).Cast<Attribute>().ToList()
                });
            }

            return properties;
        }

        #endregion

        #region set methods

        /// <summary>
        /// Set property value
        /// </summary>
        /// <typeparam name="T">Type of property</typeparam>
        /// <param name="self">Property object</param>
        /// <param name="propertyName">Name of property</param>
        /// <param name="newValue">New property value</param>
        public static void SetProperty<T>(this T self, string propertyName, object newValue) where T : class
        {
            if (string.IsNullOrEmpty(propertyName))
                return; //return if property name is null or empty

            //get property type
            Type type = self.GetType();

            //get property object
            var property = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy).FirstOrDefault(x => string.Equals(x.Name, propertyName));
            if (property == null || !property.CanWrite)
                return;

            //if new value is null set default value based on property type
            if (newValue == null)
                newValue = property.PropertyType.GetDefaultValue();

            //set property value
            property.SetValue(self, newValue, null);
        }

        #endregion

        #region mix methods

        /// <summary>
        /// Copy property to other property with same type
        /// </summary>
        /// <typeparam name="T">Type of property</typeparam>
        /// <param name="self">Property from copy</param>
        /// <param name="target">Property to copy</param>
        /// <param name="environment">Typ of environment</param>
        public static void CopyProperties<T>(this T self, T target, SerializationEnvironment environment) where T : class
        {
            if (target == null)
                return; //return if target is null

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy;

            foreach (var self_property in self.GetType().GetProperties(flags))
            {
                if (!self_property.CanRead)
                    continue;   //skip if property is not readable

                //get property object
                var target_property = target.GetType().GetProperties(flags).FirstOrDefault(x => string.Equals(x.Name, self_property.Name));
                if (target_property == null || !target_property.CanRead || !target_property.CanWrite)
                    continue;

                //get self property value
                object self_value = environment == SerializationEnvironment.iOS
                    ? self_property.GetGetMethod().Invoke(self, null)
                    : self_property.GetValue(self, null);

                //get target property value
                object target_value = environment == SerializationEnvironment.iOS
                    ? target_property.GetGetMethod().Invoke(target, null)
                    : target_property.GetValue(target, null);

                //if target property value is null set default value based on property type
                if (target_value != null)
                    target_value.DisposeEx();

                //set self value to target property
                target_property.SetValue(target_property, Convert.ChangeType(self_value, target_property.PropertyType), null);
            }
        }

        #endregion
    }
}