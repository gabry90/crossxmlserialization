﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

using System;
using System.Text.RegularExpressions;

namespace CrossXmlSerialization.Extensions
{
    /// <summary>
    /// String extension class
    /// </summary>
    internal static class StringExtensions
    {
        /// <summary>
        /// Check if type is numeric
        /// </summary>
        /// <param name="type">Type to check</param>
        /// <returns>True if numeric, False else</returns>
        public static bool IsNumeric(this Type type)
        {
            return type.IsValueType && type != typeof(bool) && type != typeof(Enum) && type != typeof(DateTime);
        }

        /// <summary>
        /// Make string to one line
        /// </summary>
        /// <param name="self">Target string</param>
        /// <param name="replacedChar">char to replace carriage return (default backspace)</param>
        /// <returns>new string in line</returns>
        public static string InLine(this string self, string replacedChar = " ")
        {
            if (replacedChar == null
                || string.Equals(replacedChar, Environment.NewLine)
                || string.Equals(replacedChar, "\n")
                || replacedChar.Contains(Environment.NewLine)
                || replacedChar.Contains("\n")
                || replacedChar.Contains("\r"))
                replacedChar = " ";
            return Regex.Replace(self, @"[\f\n\t\v\r]", replacedChar);
        }
    }
}
