﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

using System;
using System.Globalization;
using CrossXmlSerialization.Extensions;

namespace CrossXmlSerialization.Converters
{
    /// <summary>
    /// Date time converter class
    /// </summary>
    public class CrossXmlDateTimeConverter : CrossXmlBaseConverter
    {
        public string format;
        private const string DEFAULT_FORMAT = "dd/MM/yyyy hh:mm:ss";

        public CrossXmlDateTimeConverter() { }

        public CrossXmlDateTimeConverter(string format)
        {
            this.format = format;
        }

        public override string ConvertToXml(object value, Type type)
        {
            if (value == null
               || !(type == typeof(DateTime)
                    || type == typeof(DateTime?)))
                return null;

            if (type == typeof(DateTime))
                return ((DateTime)value).ToString(string.IsNullOrEmpty(format) ? DEFAULT_FORMAT : format, CultureInfo.InvariantCulture);

            if (type == typeof(DateTime?))
                return ((DateTime?)value).HasValue ? ((DateTime?)value).Value.ToString(string.IsNullOrEmpty(format) ? DEFAULT_FORMAT : format, CultureInfo.InvariantCulture) : null;

            return null;
        }

        public override object ConvertFromXml(string value, Type type)
        {
            if (string.IsNullOrEmpty(value) 
                || !(type == typeof(DateTime)
                    || type == typeof(DateTime?)))
                return type.GetDefaultValue();

            DateTime dateTime = DateTime.Now;

            if (type == typeof(DateTime))
                return DateTime.TryParse(value, out dateTime) ? dateTime : type.GetDefaultValue();

            if (type == typeof(DateTime?))
                return DateTime.TryParse(value, out dateTime) ? (DateTime?)dateTime : null;

            return type.GetDefaultValue();
        }
    }
}