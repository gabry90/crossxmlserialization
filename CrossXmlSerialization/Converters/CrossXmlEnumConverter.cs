﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

using System;
using CrossXmlSerialization.Extensions;

namespace CrossXmlSerialization.Converters
{
    /// <summary>
    /// Enum converter class
    /// </summary>
    public class CrossXmlEnumConverter : CrossXmlBaseConverter
    {
        public string format;

        public CrossXmlEnumConverter() { }

        public CrossXmlEnumConverter(string format)
        {
            this.format = format;
        }

        public override string ConvertToXml(object value, Type type)
        {
            if (value == null || !type.IsEnum)
                return null;

            return string.IsNullOrEmpty(format) ? value.ToString() : Enum.Format(type, value, format);
        }

        public override object ConvertFromXml(string value, Type type)
        {
            if (string.IsNullOrEmpty(value) || !type.IsEnum)
                return type.GetDefaultValue();

            object en = null;
            try { en = Enum.Parse(type, value); }
            catch { en = null; }

            return en == null ? type.GetDefaultValue() : en;
        }
    }
}