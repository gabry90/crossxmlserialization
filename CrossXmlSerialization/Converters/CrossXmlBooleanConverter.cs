﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

using System;
using CrossXmlSerialization.Extensions;

namespace CrossXmlSerialization.Converters
{
    /// <summary>
    /// Boolean converter class
    /// </summary>
    public class CrossXmlBooleanConverter : CrossXmlBaseConverter
    {
        public CrossXmlBooleanConverter() { }

        public override string ConvertToXml(object value, Type type)
        {
            if (value == null || type != typeof(bool))
                return null;

            return (bool)value ? "True" : "False";
        }

        public override object ConvertFromXml(string value, Type type)
        {
            if (type != typeof(bool))
                return type.GetDefaultValue();

            return !string.IsNullOrEmpty(value) 
                && string.Equals(value, "True") 
                ? true 
                : false;
        }
    }
}