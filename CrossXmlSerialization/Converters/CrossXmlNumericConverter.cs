﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

using System;
using CrossXmlSerialization.Extensions;

namespace CrossXmlSerialization.Converters
{
    /// <summary>
    /// Numeric converter class
    /// </summary>
    public class CrossXmlNumericConverter : CrossXmlBaseConverter
    {
        private string format;

        public CrossXmlNumericConverter() { }

        public CrossXmlNumericConverter(string format)
        {
            this.format = format;
        }

        public override string ConvertToXml(object value, Type type)
        {
            if (value == null || !type.IsNumeric())
                return null;

            if (string.IsNullOrEmpty(format))
                return value.ToString();
            else
            {
                if (type == typeof(short))
                    return ((short)value).ToString(format);
                else if (type == typeof(int))
                    return ((int)value).ToString(format);
                else if (type == typeof(long))
                    return ((long)value).ToString(format);
                else if (type == typeof(decimal))
                    return ((decimal)value).ToString(format);
                else if (type == typeof(float))
                    return ((float)value).ToString(format);
                else
                    return value.ToString();
            }
        }

        public override object ConvertFromXml(string value, Type type)
        {
            if (string.IsNullOrEmpty(value) || !type.IsNumeric())
                return type.GetDefaultValue();

            try
            {
                if (type == typeof(short))
                    return Convert.ToInt16(value);
                else if (type == typeof(int))
                    return Convert.ToInt32(value);
                else if (type == typeof(long))
                    return Convert.ToInt64(value);
                else if (type == typeof(decimal))
                    return Convert.ToDecimal(value);
                else if (type == typeof(float))
                    return Convert.ToSingle(type);
                else
                    return type.GetDefaultValue();
            }
            catch
            {
                return type.GetDefaultValue();
            }
        }
    }
}