﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

using System;
using System.Text;
using CrossXmlSerialization.Extensions;


namespace CrossXmlSerialization.Converters
{
    /// <summary>
    /// Date time converter class
    /// </summary>
    public class CrossXmlTimeSpanConverter : CrossXmlBaseConverter
    {
        public string format;
        private const string DEFAULT_FORMAT = "dd:hh:mm:ss";

        public CrossXmlTimeSpanConverter() { }

        public CrossXmlTimeSpanConverter(string format)
        {
            this.format = format;
        }

        public override string ConvertToXml(object value, Type type)
        {
            if (value == null 
                || !(type == typeof(TimeSpan) 
                    || type == typeof(TimeSpan?)))
                return null;

            if (type == typeof(TimeSpan))
                return ConvertCustomFormatToString((TimeSpan)value, string.IsNullOrEmpty(format) ? DEFAULT_FORMAT : format);

            if (type == typeof(TimeSpan?))
                return ((TimeSpan?)value).HasValue ? ConvertCustomFormatToString(((TimeSpan?)value).Value, string.IsNullOrEmpty(format) ? DEFAULT_FORMAT : format) : null;

            return null;
        }

        public override object ConvertFromXml(string value, Type type)
        {
            if (string.IsNullOrEmpty(value)
                || !(type == typeof(TimeSpan)
                    || type == typeof(TimeSpan?)))
                return type.GetDefaultValue();

            TimeSpan timeSpan = TimeSpan.Zero;

            if (type == typeof(TimeSpan))
                return TimeSpan.TryParse(value, out timeSpan) ? timeSpan : type.GetDefaultValue();

            if (type == typeof(TimeSpan?))
                return TimeSpan.TryParse(value, out timeSpan) ? (TimeSpan?)timeSpan : null;

            return type.GetDefaultValue();
        }

        private string ConvertCustomFormatToString(TimeSpan span, string format)
        {
            StringBuilder sb = new StringBuilder(format);
            if (format.Contains("dd")) sb = sb.Replace("dd", span.Days.ToString("0#"));
            if (format.Contains("hh")) sb = sb.Replace("hh", span.Hours.ToString("0#"));
            if (format.Contains("mm")) sb = sb.Replace("mm", span.Minutes.ToString("0#"));
            if (format.Contains("ss")) sb = sb.Replace("ss", span.Seconds.ToString("0#"));
            return sb.ToString();
        }
    }
}