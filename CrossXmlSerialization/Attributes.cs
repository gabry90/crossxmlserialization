﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

using System;

namespace CrossXmlSerialization
{
    /// <summary>
    /// Base Attribute class
    /// </summary>
    public abstract class CrossXmlBaseAttribute : Attribute
    {
        public CrossXmlBaseAttribute(bool allowNullValue)
        {
            AllowNullValue = allowNullValue;
        }

        public bool AllowNullValue { get; private set; }
    }

    /// <summary>
    /// Implement this attribute for ignoring property from serialization
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class CrossXmlIgnoreAttribute : Attribute { }

    /// <summary>
    /// Implement this attribute for define root node
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class CrossXmlRootAttribute : CrossXmlBaseAttribute
    {
        public CrossXmlRootAttribute(string rootName, Type rootType, bool allowNullValue = false)
            : base(allowNullValue)
        {
            RootName = rootName;
            RootType = rootType;
        }

        public string RootName { get; private set; }
        public Type RootType { get; private set; }
    }

    /// <summary>
    /// Implement this attribute for Attribute xml node
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class CrossXmlAttributeAttribute : CrossXmlBaseAttribute
    {
        public CrossXmlAttributeAttribute(string attributeName, Type attributeType, bool allowNullValue = false)
            : base(allowNullValue)
        {
            AttributeName = attributeName;
            AttributeType = attributeType;
        }

        public string AttributeName { get; private set; }
        public Type AttributeType { get; private set; }
    }

    /// <summary>
    /// Implement this attribute for Element attribute node
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class CrossXmlElementAttribute : CrossXmlBaseAttribute
    {
        public CrossXmlElementAttribute(string elementName, Type elementType, bool allowNullValue = false)
            : base(allowNullValue)
        {
            ElementName = elementName;
            ElementType = elementType;
        }

        public string ElementName { get; private set; }
        public Type ElementType { get; private set; }
    }

    /// <summary>
    /// Implement this attribute for define List property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class CrossXmlListAttribute : CrossXmlBaseAttribute
    {
        public CrossXmlListAttribute(string listName, bool allowNullValue = false)
             : base(allowNullValue)
        {
            ListName = listName;
        }

        public string ListName { get; private set; }
    }

    /// <summary>
    /// Implement this attribute for define List item property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
    public class CrossXmlListItemAttribute : Attribute
    {
        public CrossXmlListItemAttribute(string itemName, Type itemType)
        {
            ItemName = itemName;
            ItemType = itemType;
        }

        public string ItemName { get; private set; }
        public Type ItemType { get; private set; }
    }

    /// <summary>
    /// Implement this attribute for custom conversion to / from xml property value
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class CrossXmlConverterAttribute : Attribute
    {
        public CrossXmlConverterAttribute(Type converterType, params object[] converterParams)
        {
            ConverterType = converterType;
            ConverterParams = converterParams;
        }

        public Type ConverterType { get; private set; }
        public object[] ConverterParams { get; private set; }
    }
}