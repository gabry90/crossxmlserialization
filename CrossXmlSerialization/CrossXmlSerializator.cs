﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

namespace CrossXmlSerialization
{
    /// <summary>
    /// Serialization environment types
    /// </summary>
    public enum SerializationEnvironment
    {
        Windows,
        Mac,
        iOS
    }

    public sealed class CrossXmlSerializator
    {
        #region vars

        public SerializationEnvironment Environment;

        #endregion

        #region constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public CrossXmlSerializator()
        {
            Environment = SerializationEnvironment.Windows;
        }

        /// <summary>
        /// Constructor with serialization environment params
        /// </summary>
        /// <param name="environment">Type of environment</param>
        public CrossXmlSerializator(SerializationEnvironment environment)
        {
            Environment = environment;
        }

        #endregion

        #region serialization methods

        /// <summary>
        /// Serialize [class to xml]
        /// </summary>
        /// <typeparam name="T">Type of class</typeparam>
        /// <param name="obj">Class object</param>
        /// <returns>xml</returns>
        public string Serialize<T>(T obj) where T : class
        {
            if (obj == null)
                return null;

            return new SerializeProcessor<T>(Environment).ProcessObject(obj);
        }

        /// <summary>
        /// Deserialize [xml to class]
        /// </summary>
        /// <typeparam name="T">Type of class</typeparam>
        /// <param name="xml">xml to deserialize</param>
        /// <returns>Class deserialized</returns>
        public T Deserialize<T>(string xml) where T : class
        {
            if (string.IsNullOrEmpty(xml))
                return null;

            return new SerializeProcessor<T>(Environment).ProcessXml(xml);
        }

        #endregion
    }
}