﻿/*
------------------------------------------------
	<one line to give the program's name and a brief idea of what it does.>
	Copyright (C) 2016 Gindro Gabriele

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------
*/

using System;
using System.Linq;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;
using CrossXmlSerialization.Converters;
using CrossXmlSerialization.Extensions;

namespace CrossXmlSerialization
{
    /// <summary>
    /// Serialize processor class
    /// </summary>
    /// <typeparam name="T">Type of class to serialize / deserialize</typeparam>
    internal class SerializeProcessor<T> where T : class
    {
        #region vars

        private SerializationEnvironment Environment;

        /// <summary>
        /// Check if class is serializable
        /// </summary>
        public bool IsSerializable
        {
            get { return typeof(T).GetAttributeInfo<SerializableAttribute>() != null; }
        }

        /// <summary>
        /// Check if class is root node
        /// </summary>
        public bool IsRoot
        {
            get { return typeof(T).GetAttributeInfo<CrossXmlRootAttribute>() != null; }
        }

        #endregion

        #region constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="environment">Type of environment</param>
        public SerializeProcessor(SerializationEnvironment environment)
        {
            Environment = environment;
        }

        #endregion

        #region public methods

        /// <summary>
        /// Process instance object and return xml
        /// </summary>
        /// <param name="instance">Instance to process</param>
        /// <returns>xml processed</returns>
        public string ProcessObject(T instance)
        {
            if (!IsSerializable || instance == null)
                return null;

            string name = typeof(T).GetAttributeInfo<CrossXmlRootAttribute>().RootName;
            return ParseObject(new XElement(string.IsNullOrEmpty(name) ? typeof(T).Name : name), instance);
        }

        /// <summary>
        /// Process xml and return instance
        /// </summary>
        /// <param name="xml">Xml to process</param>
        /// <returns>Instance of Type T processed</returns>
        public T ProcessXml(string xml)
        {
            if (string.IsNullOrEmpty(xml))
                return null;

            return (T)ParseXml(xml, Activator.CreateInstance<T>());
        }

        #endregion

        #region private methods

        /// <summary>
        /// Parse object and return xml
        /// </summary>
        /// <param name="node">Xml node</param>
        /// <param name="target">Target object</param>
        /// <returns>xml parsed</returns>
        private string ParseObject(XElement node, object target)
        {
            if (node == null || target == null)
                return null;    //skip if node or target is null

            //get properties
            List<CustomPropertyInfo> properties = target.GetPropertiesExcludingAttributes(Environment, typeof(CrossXmlIgnoreAttribute));

            foreach (CustomPropertyInfo propertyInfo in properties)
            {
                if (propertyInfo.Attributes.Count == 0)
                    continue;

                //get converter
                CrossXmlBaseConverter converter = GetConverter(propertyInfo);

                //get element attribute
                CrossXmlElementAttribute elementAttribute = (CrossXmlElementAttribute)propertyInfo.Attributes.FirstOrDefault(x => x.GetType() == typeof(CrossXmlElementAttribute));
                if (elementAttribute != null)
                {
                    string name = string.IsNullOrEmpty(elementAttribute.ElementName) ? propertyInfo.Name : elementAttribute.ElementName;

                    if (propertyInfo.Value == null)
                    {   
                        if (elementAttribute.AllowNullValue)
                            node.Add(new XElement(name));
                    }
                    else
                    {
                        if (propertyInfo.IsValueType)
                            node.Add(new XElement(name, converter != null ? converter.ConvertToXml(propertyInfo.Value, elementAttribute.ElementType) : propertyInfo.Value));
                        else
                        {
                            XElement xml = null;
                            if (propertyInfo.Type.IsInterface)
                            {
                                if (elementAttribute.ElementType.GetInterfaces().Contains(propertyInfo.Type))
                                    xml = converter != null ? new XElement(name, converter.ConvertToXml(propertyInfo.Value, elementAttribute.ElementType)) : XElement.Parse(ParseObject(new XElement(name), propertyInfo.Value));
                                else
                                    continue;
                            }
                            else
                                xml = converter != null ? new XElement(name, converter.ConvertToXml(propertyInfo.Value, propertyInfo.Type)) : XElement.Parse(ParseObject(new XElement(name), propertyInfo.Value));

                            if (xml != null)
                                node.Add(xml);
                        } 
                    }
                    continue;
                }

                //get attribute attribute
                CrossXmlAttributeAttribute attributeAttribute = (CrossXmlAttributeAttribute)propertyInfo.Attributes.FirstOrDefault(x => x.GetType() == typeof(CrossXmlAttributeAttribute));
                if (attributeAttribute != null && propertyInfo.IsValueType)
                {
                    string name = string.IsNullOrEmpty(attributeAttribute.AttributeName) ? propertyInfo.Name : attributeAttribute.AttributeName;

                    if (propertyInfo.Value == null)
                    {
                        if (attributeAttribute.AllowNullValue)
                            node.Add(new XAttribute(name, ""));
                    }
                    else
                        node.Add(new XAttribute(name, converter != null ? converter.ConvertToXml(propertyInfo.Value, attributeAttribute.AttributeType) : propertyInfo.Value));
                    
                    continue;
                }

                //get list attribute
                CrossXmlListAttribute listAttribute = (CrossXmlListAttribute)propertyInfo.Attributes.FirstOrDefault(x => x.GetType() == typeof(CrossXmlListAttribute));
                if (listAttribute != null && propertyInfo.IsList)
                {
                    List<CrossXmlListItemAttribute> listItemAttribute = propertyInfo.Attributes.Where(x => x.GetType() == typeof(CrossXmlListItemAttribute)).Cast<CrossXmlListItemAttribute>().ToList();
                    if (listItemAttribute != null)
                    {
                        string name = string.IsNullOrEmpty(listAttribute.ListName) ? propertyInfo.Name : listAttribute.ListName;
                        if (propertyInfo.Value == null)
                        {
                            if (listAttribute.AllowNullValue)
                                node.Add(new XElement(name));
                            continue;
                        }

                        XElement list = new XElement(name);

                        if (((IList)propertyInfo.Value).Count == 0)
                        {
                            //skip empty list
                            node.Add(list);
                            continue;   
                        }

                        //get collection item attributes
                        foreach (CrossXmlListItemAttribute itemAttribute in listItemAttribute)
                        {
                            //excluding if item type is not inerithed or not equals from collection type
                            Type baseType = propertyInfo.Type.GetBaseType();
                            if (baseType == null || !itemAttribute.ItemType.IsSameOrSubClass(baseType))
                                continue;

                            foreach (var item in ((IList)propertyInfo.Value))
                            {
                                if (item == null)
                                    continue;

                                string itemName = string.IsNullOrEmpty(itemAttribute.ItemName) ? item.GetType().Name : itemAttribute.ItemName;

                                if (item.GetType() == itemAttribute.ItemType)
                                    list.Add(XElement.Parse(ParseObject(new XElement(itemName), item)));
                            }
                        }
                        node.Add(list);
                    }
                }
            }

            return node.ToString();
        }

        /// <summary>
        /// Parse xml and return object target
        /// </summary>
        /// <param name="xml">Xml part</param>
        /// <param name="target">Obejct target</param>
        /// <returns>Object parsed from xml</returns>
        private object ParseXml(string xml, object target)
        {
            if (string.IsNullOrEmpty(xml))
                return null;    //return null if xml is null

            //parse xml
            XElement node = XElement.Parse(xml);

            //get properties
            List<CustomPropertyInfo> properties = target.GetPropertiesExcludingAttributes(Environment, typeof(CrossXmlIgnoreAttribute));

            //parse attributes
            foreach (XAttribute attribute in node.Attributes())
            {
                CustomPropertyInfo propertyInfo = GetPropertyInfoAssociated(attribute.Name.LocalName, properties);
                if (propertyInfo != null && attribute.Value != null && propertyInfo.IsValueType)
                {
                    //get converter
                    CrossXmlBaseConverter converter = GetConverter(propertyInfo);

                    object value = converter != null ? converter.ConvertFromXml(attribute.Value, propertyInfo.Type) : attribute.Value;
                    try
                    {
                        if (attribute.Value != null && attribute.Value.GetType() != propertyInfo.Type && !propertyInfo.Type.IsNullable())
                            value = Convert.ChangeType(value, propertyInfo.Type);
                    }
                    catch { value = null; }

                    target.SetProperty(propertyInfo.Name, value);
                }  
            }

            //parse elements
            foreach (XElement element in node.Elements())
            {
                CustomPropertyInfo propertyInfo = GetPropertyInfoAssociated(element.Name.LocalName, properties);
                if (propertyInfo != null && element.Value != null)
                {
                    if (propertyInfo.IsList)
                    {
                        object list = Activator.CreateInstance(propertyInfo.Type);
                        List<CrossXmlListItemAttribute> itemAttributes = propertyInfo.Attributes.Where(x => x.GetType() == typeof(CrossXmlListItemAttribute)).Cast<CrossXmlListItemAttribute>().ToList();
                        
                        foreach (XElement item in element.Elements())
                        {
                            CrossXmlListItemAttribute itemAttribute = itemAttributes.FirstOrDefault(x =>
                                    (string.IsNullOrEmpty(x.ItemName) && string.Equals(x.ItemType.Name, item.Name.LocalName))
                                || (!string.IsNullOrEmpty(x.ItemName) && string.Equals(x.ItemName, item.Name.LocalName)));

                            Type baseType = propertyInfo.Type.GetBaseType();
                            if (baseType != null && itemAttribute != null && itemAttribute.ItemType.IsSameOrSubClass(baseType))
                                ((IList)list).Add(ParseXml(item.ToString(), Activator.CreateInstance(itemAttribute.ItemType)));
                        }

                        target.SetProperty(propertyInfo.Name, list);
                    }
                    else
                    {
                        //get converter
                        CrossXmlBaseConverter converter = GetConverter(propertyInfo);
                        if (propertyInfo.IsValueType)
                        {
                            object value = converter != null ? converter.ConvertFromXml(element.Value, propertyInfo.Type) : element.Value;
                            try
                            {
                                if (element.Value != null && element.Value.GetType() != propertyInfo.Type && !propertyInfo.Type.IsNullable())
                                    value = Convert.ChangeType(value, propertyInfo.Type);
                            }
                            catch { value = null; }

                            target.SetProperty(propertyInfo.Name, value);
                        }
                        else
                        {
                            //get attribute element
                            CrossXmlElementAttribute attribute = (CrossXmlElementAttribute)propertyInfo.Attributes.FirstOrDefault(x => x.GetType() == typeof(CrossXmlElementAttribute));
                            if (attribute == null || attribute.ElementType == null)
                                continue;

                            object obj = null;
                            if (propertyInfo.Type.IsInterface)
                            {
                                if (attribute.ElementType.GetInterfaces().Contains(propertyInfo.Type))
                                    obj = converter != null ? converter.ConvertFromXml(element.Value, attribute.ElementType) : ParseXml(element.ToString(), Activator.CreateInstance(attribute.ElementType));
                                else
                                    continue;
                            }
                            else
                                obj = converter != null ? converter.ConvertFromXml(element.Value, propertyInfo.Type) : ParseXml(element.ToString(), Activator.CreateInstance(propertyInfo.Type));

                            target.SetProperty(propertyInfo.Name, obj);  
                        }
                    }
                }
            }

            return target;
        }

        /// <summary>
        /// Get converter
        /// </summary>
        /// <param name="propertyInfo">Cusatom property info class</param>
        /// <returns>Converter implemented</returns>
        private CrossXmlBaseConverter GetConverter(CustomPropertyInfo propertyInfo)
        {
            if (propertyInfo == null || propertyInfo.Attributes.Count == 0)
                return null;

            CrossXmlConverterAttribute converterAttribute = (CrossXmlConverterAttribute)propertyInfo.Attributes.FirstOrDefault(x => x.GetType() == typeof(CrossXmlConverterAttribute));
            return converterAttribute != null && converterAttribute.ConverterType != null
                    ? (CrossXmlBaseConverter)Activator.CreateInstance(converterAttribute.ConverterType, converterAttribute.ConverterParams.Length > 0 ? converterAttribute.ConverterParams : null)
                    : null;
        }

        /// <summary>
        /// Get custom property info associated
        /// </summary>
        /// <param name="nodeName">Name od xml node</param>
        /// <param name="properties">List ofcustom property info</param>
        /// <returns>custom property info</returns>
        private CustomPropertyInfo GetPropertyInfoAssociated(string nodeName, List<CustomPropertyInfo> properties)
        {
            if (properties == null || properties.Count == 0)
                return null;

            foreach (CustomPropertyInfo propertyInfo in properties)
            {
                if (propertyInfo.Attributes.Exists(x => x.GetType() == typeof(CrossXmlIgnoreAttribute)))
                    continue;

                CrossXmlElementAttribute elementAttribute = (CrossXmlElementAttribute)propertyInfo.Attributes.FirstOrDefault(x => x.GetType() == typeof(CrossXmlElementAttribute));
                if (elementAttribute != null)
                {
                    if (string.IsNullOrEmpty(elementAttribute.ElementName) && string.Equals(propertyInfo.Name, nodeName))
                        return propertyInfo;

                    if (!string.IsNullOrEmpty(elementAttribute.ElementName) && string.Equals(elementAttribute.ElementName, nodeName))
                        return propertyInfo;

                    continue;
                }

                CrossXmlAttributeAttribute attributeAttribute = (CrossXmlAttributeAttribute)propertyInfo.Attributes.FirstOrDefault(x => x.GetType() == typeof(CrossXmlAttributeAttribute));
                if (attributeAttribute != null && propertyInfo.IsValueType)
                {
                    if (string.IsNullOrEmpty(attributeAttribute.AttributeName) && string.Equals(propertyInfo.Name, nodeName))
                        return propertyInfo;

                    if (!string.IsNullOrEmpty(attributeAttribute.AttributeName) && string.Equals(attributeAttribute.AttributeName, nodeName))
                        return propertyInfo;

                    continue;
                }

                CrossXmlListAttribute listAttribute = (CrossXmlListAttribute)propertyInfo.Attributes.FirstOrDefault(x => x.GetType() == typeof(CrossXmlListAttribute));
                if (listAttribute != null && propertyInfo.IsList)
                {
                    if (propertyInfo.Attributes.Where(x => x.GetType() == typeof(CrossXmlListItemAttribute)).Cast<CrossXmlListItemAttribute>().Count() == 0)
                        continue;

                    if (string.IsNullOrEmpty(listAttribute.ListName) && string.Equals(propertyInfo.Name, nodeName))
                        return propertyInfo;

                    if (!string.IsNullOrEmpty(listAttribute.ListName) && string.Equals(listAttribute.ListName, nodeName))
                        return propertyInfo;
                }
            }

            return null;
        }

        #endregion
    }
}