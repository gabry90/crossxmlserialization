﻿namespace CrossXmlSerialization.Test
{
    partial class MainTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serialize_button = new System.Windows.Forms.Button();
            this.deserialize_button = new System.Windows.Forms.Button();
            this.xml_text = new System.Windows.Forms.RichTextBox();
            this.clear_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // serialize_button
            // 
            this.serialize_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.serialize_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialize_button.Location = new System.Drawing.Point(12, 28);
            this.serialize_button.Name = "serialize_button";
            this.serialize_button.Size = new System.Drawing.Size(122, 36);
            this.serialize_button.TabIndex = 0;
            this.serialize_button.Text = "Serialize";
            this.serialize_button.UseVisualStyleBackColor = true;
            this.serialize_button.Click += new System.EventHandler(this.serialize_button_Click);
            // 
            // deserialize_button
            // 
            this.deserialize_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deserialize_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deserialize_button.Location = new System.Drawing.Point(12, 84);
            this.deserialize_button.Name = "deserialize_button";
            this.deserialize_button.Size = new System.Drawing.Size(122, 36);
            this.deserialize_button.TabIndex = 1;
            this.deserialize_button.Text = "Deserialize";
            this.deserialize_button.UseVisualStyleBackColor = true;
            this.deserialize_button.Click += new System.EventHandler(this.deserialize_button_Click);
            // 
            // xml_text
            // 
            this.xml_text.BackColor = System.Drawing.Color.Gainsboro;
            this.xml_text.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.xml_text.Location = new System.Drawing.Point(153, 28);
            this.xml_text.Name = "xml_text";
            this.xml_text.Size = new System.Drawing.Size(368, 427);
            this.xml_text.TabIndex = 2;
            this.xml_text.Text = "";
            // 
            // clear_button
            // 
            this.clear_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.clear_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear_button.Location = new System.Drawing.Point(12, 419);
            this.clear_button.Name = "clear_button";
            this.clear_button.Size = new System.Drawing.Size(122, 36);
            this.clear_button.TabIndex = 3;
            this.clear_button.Text = "Clear";
            this.clear_button.UseVisualStyleBackColor = true;
            this.clear_button.Click += new System.EventHandler(this.clear_button_Click);
            // 
            // MainTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 471);
            this.Controls.Add(this.clear_button);
            this.Controls.Add(this.xml_text);
            this.Controls.Add(this.deserialize_button);
            this.Controls.Add(this.serialize_button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cross Xml Serialization Test";
            this.Load += new System.EventHandler(this.MainTest_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button serialize_button;
        private System.Windows.Forms.Button deserialize_button;
        private System.Windows.Forms.RichTextBox xml_text;
        private System.Windows.Forms.Button clear_button;
    }
}

