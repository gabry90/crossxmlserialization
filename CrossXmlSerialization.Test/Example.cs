﻿using System;
using CrossXmlSerialization;
using System.Collections.Generic;
using CrossXmlSerialization.Converters;
using System.Drawing;

namespace CrossXmlSerialization.Test
{
    /***********************************************************************
     TESTING EXAMPLE DTO
    ***********************************************************************/

    public interface ITest { }

    [Serializable]
    [CrossXmlRoot("example", typeof(ExampleDto))]
    public class ExampleDto
    {
        public static string Serialize(ExampleDto dto)
        {
            return new CrossXmlSerializator().Serialize(dto);
        }

        public static ExampleDto Deserialize(string xml)
        {
            return new CrossXmlSerializator().Deserialize<ExampleDto>(xml);
        }

        [CrossXmlIgnore]
        public string Field_Ignored { get; set; }

        [CrossXmlAttribute("field_1", typeof(string))]
        public string Field_1 { get; set; }

        [CrossXmlAttribute("field_2", typeof(int))]
        public int Field_2 { get; set; }

        [CrossXmlAttribute("field_3", typeof(bool))]
        public bool Field_3 { get; set; }

        [CrossXmlAttribute("field_4", typeof(decimal))]
        public decimal Field_4 { get; set; }

        [CrossXmlAttribute("image", typeof(Image))]
        public Image Image_prova { get; set; }

        [CrossXmlList("items", true)]
        [CrossXmlListItem("item1", typeof(ExampleItem1Dto))]
        [CrossXmlListItem("item2", typeof(ExampleItem2Dto))]
        public List<ExampleItemBaseDto> Items { get; set; }

        [CrossXmlElement("internal_item", typeof(ExampleInternalDto))]
        internal ExampleInternalDto InternalDto { get; set; }

        [CrossXmlElement("interface", typeof(ExampleInterface))]
        internal ITest Interface { get; set; }

        [CrossXmlElement("interface_reverse", typeof(ITest))]
        internal ExampleInterfaceReverse InterfaceReverse { get; set; }
    }

    /***********************************************************************
     TESTING HIERARCHY COLLECTION ITEMS
    ***********************************************************************/

    [Serializable]
    public class ExampleItemBaseDto
    {
        [CrossXmlAttribute("id", typeof(int))]
        public int ID { get; set; }
    }

    /***********************************************************************
     TESTING PROPERTY ACCESS MODIFIERS 
    ***********************************************************************/

    [Serializable]
    public class ExampleItem1Dto : ExampleItemBaseDto
    {
        [CrossXmlElement("field_1", typeof(string))]
        private string Field_1 { get; set; }

        [CrossXmlElement("field_2", typeof(int))]
        protected int Field_2 { get; set; }

        [CrossXmlElement("field_3", typeof(bool))]
        internal bool Field_3 { get; set; }
    }

    /***********************************************************************
     TESTING CONVERTERS 
    ***********************************************************************/

    public enum ExampleEnum
    {
        Example1,
        Example2,
        Example3
    }

    [Serializable]
    public class ExampleItem2Dto : ExampleItemBaseDto
    {
        [CrossXmlElement("field_4", typeof(ExampleEnum))]
        [CrossXmlConverter(typeof(CrossXmlEnumConverter))]
        internal ExampleEnum Field_4 { private get; set; }

        [CrossXmlElement("field_5", typeof(int))]
        [CrossXmlConverter(typeof(CrossXmlNumericConverter))]
        protected int Field_5 { get; set; }

        [CrossXmlElement("field_6", typeof(bool))]
        [CrossXmlConverter(typeof(CrossXmlBooleanConverter))]
        internal bool Field_6 { get; set; }

        [CrossXmlAttribute("field_7", typeof(DateTime))]
        [CrossXmlConverter(typeof(CrossXmlDateTimeConverter), "dd/MM/yyyy hh:mm:ss")]
        public DateTime? Field_7 { get; set; }
    }

    /***********************************************************************
      TESTING PROPERTY ACCESS MODIFIERS AND INTERNAL CLASS ACCESS MODIFIER
    ***********************************************************************/

    [Serializable]
    internal class ExampleInternalDto
    {
        [CrossXmlAttribute("field_1", typeof(string))]
        public string Field_1 { get; private set; }

        [CrossXmlElement("field_2", typeof(int))]
        public int Field_2 { get; protected set; }

        [CrossXmlAttribute("field_3", typeof(bool))]
        public bool Field_3 { private get; set; }
    }

    /***********************************************************************
      TESTING INTERFACE INSTANCE
    ***********************************************************************/

    internal class ExampleInterface : ITest { }

    internal class ExampleInterfaceReverse : ITest { }
}