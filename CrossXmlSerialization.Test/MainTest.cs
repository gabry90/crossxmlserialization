﻿using System;
using System.Windows.Forms;
using CrossXmlSerialization;
using System.Collections.Generic;
using System.Drawing;

namespace CrossXmlSerialization.Test
{
    public partial class MainTest : Form
    {
        public MainTest()
        {
            InitializeComponent();
        }

        private void MainTest_Load(object sender, EventArgs e)
        {

        }

        private void serialize_button_Click(object sender, EventArgs e)
        {
            ExampleDto example = new ExampleDto();
            example.Field_Ignored = "prova ignorante";
            example.Field_1 = "prova";
            example.Field_2 = 15;
            example.Field_3 = true;
            example.Field_4 = 18000;
            example.Image_prova = Image.FromFile("0.png");

            List<ExampleItemBaseDto> exampleCollection =  new List<ExampleItemBaseDto>();

            ExampleItem1Dto exampleItem1 = new ExampleItem1Dto();
            exampleItem1.ID = 1;
            exampleItem1.Field_3 = true;

            ExampleItem2Dto exampleItem2 = new ExampleItem2Dto();
            exampleItem2.ID = 2;
            exampleItem2.Field_4 = ExampleEnum.Example2;
            exampleItem2.Field_6 = true;
            exampleItem2.Field_7 = DateTime.Now;

            exampleCollection.Add(exampleItem1);
            exampleCollection.Add(exampleItem2);

            example.Items = exampleCollection;

            ExampleInternalDto exampleInternal = new ExampleInternalDto();
            exampleInternal.Field_3 = true;

            example.InternalDto = exampleInternal;

            example.Interface = new ExampleInterface();
            example.InterfaceReverse = new ExampleInterfaceReverse();

            try
            {
                xml_text.Text = ExampleDto.Serialize(example);
                xml_text.ForeColor = Color.Black;
                xml_text.ReadOnly = false;
            }
            catch (Exception ex)
            {
                xml_text.Text = ex.ToString();
                xml_text.ForeColor = Color.Red;
                xml_text.ReadOnly = true;
            }
        }

        private void deserialize_button_Click(object sender, EventArgs e)
        {
            try
            {
                ExampleDto example = ExampleDto.Deserialize(xml_text.Text);
                MessageBox.Show("Success!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                xml_text.Text = ex.ToString();
                xml_text.ForeColor = Color.Red;
                xml_text.ReadOnly = true;
            }
        }

        private void clear_button_Click(object sender, EventArgs e)
        {
            xml_text.Text = "";
            xml_text.ForeColor = Color.Black;
            xml_text.ReadOnly = false;
        }
    }
}
